//
//  ViewController.swift
//  NotasCoreData
//
//  Created by Fran on 9/1/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var texto: UITextView!
    @IBOutlet weak var fecha: UILabel!
    @IBOutlet weak var info: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func nuevaNota(_ sender: Any) {
        self.texto.text = ""
        self.fecha.text = ""
        self.info.text = ""
    }
    
    @IBAction func guardar(_ sender: Any) {
        
        if let miDelegate = UIApplication.shared.delegate as? AppDelegate {
            let miContexto = miDelegate.persistentContainer.viewContext
            let nuevaNota = NSEntityDescription.insertNewObject(forEntityName: "Nota",
                                                                into: miContexto)
            nuevaNota.setValue(self.texto.text, forKey: "texto")
            nuevaNota.setValue(Date(), forKey: "fecha")
            do {
                try miContexto.save()
            } catch {
                print("Error al guardar el contexto: \(error)")
            }
            self.fecha.text = DateFormatter.localizedString(
                from:Date(),
                dateStyle: .short, timeStyle: .medium)
            self.info.text = "Nota guardada"
        }
    }
}

